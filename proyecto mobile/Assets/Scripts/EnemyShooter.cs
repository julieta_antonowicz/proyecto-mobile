using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class EnemyShooter : MonoBehaviour
{
    public Proyectil projectilePrefab;
    public float fireRate = 2f;
    public float projectileSpeed = 5f;
    public Transform firePoint;

    private float nextFireTime = 0f;

    private ObjectPool<Proyectil> proyectilPool;

    private void Start()
    {
        proyectilPool = new ObjectPool<Proyectil>(() =>
        {
            Proyectil proyectil = Instantiate(projectilePrefab, firePoint.position, firePoint.rotation);
            proyectil.DesactivarProyectil(DesactivarProyectilPool);
            return proyectil;
        }, bala =>
        {
            bala.gameObject.SetActive(true);
        }, bala =>
        {
            bala.gameObject.SetActive(true);
        }, bala =>
        {
            Destroy(bala.gameObject);
        }, true, 10, 25);

    }

    private void ActivarProyectil(Proyectil proyectil)
    {
        proyectil.gameObject.SetActive(true);
    }

    void Update()
    {
        if (Time.time > nextFireTime)
        {
            Shoot();
            nextFireTime = Time.time + 1f / fireRate;
        }
    }

    void Shoot()
    {
        Proyectil projectile = proyectilPool.Get();
        Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();
        if (rb != null)
        {
            rb.velocity = firePoint.right * projectileSpeed;
        }
    }

    void DesactivarProyectilPool(Proyectil proyectil)
    {
        proyectilPool.Release(proyectil);
    }
}
