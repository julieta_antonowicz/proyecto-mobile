using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Transform startPoint;
    public Transform endPoint; 
    public float speed = 2f; 

    private Vector3 startLocation;
    private Vector3 endLocation;
    private bool movingToEnd = true;

    void Start()
    {
        startLocation = startPoint.position;
        endLocation = endPoint.position;
    }

    void Update()
    {
        Vector3 targetPosition = movingToEnd ? endLocation : startLocation;

        Vector3 direction = (targetPosition - transform.position).normalized;

        transform.Translate(direction * speed * Time.deltaTime);

        if (Vector3.Distance(transform.position, targetPosition) < 0.1f)
        {
            movingToEnd = !movingToEnd;
        }
    }
}
