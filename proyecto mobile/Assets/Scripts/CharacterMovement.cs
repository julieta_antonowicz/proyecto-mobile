using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float jumpForce = 10f;
    public float magneticForce = 10f;
    private bool moveLeft, moveRight;
    private bool isJumping = false;
    private bool isGrounded = false;
    private bool isUsingMagneticClaws = false; 
    private Animator animator;
    private SpriteRenderer spriteRenderer;
    private Rigidbody2D rb;

    private void Start()
    {
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (moveLeft)
        {
            transform.Translate(Vector2.left * moveSpeed * Time.deltaTime);
            spriteRenderer.flipX = true;
        }
        if (moveRight)
        {
            transform.Translate(Vector2.right * moveSpeed * Time.deltaTime);
            spriteRenderer.flipX = false;
        }

        if (moveLeft || moveRight)
        {
            animator.SetBool("IsRunning", true);
        }
        else
        {
            animator.SetBool("IsRunning", false);
        }

        if (isJumping && (isGrounded || isUsingMagneticClaws))
        {
            rb.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
            isJumping = false;
            isGrounded = false;
        }

        if (isUsingMagneticClaws)
        {
            ApplyMagneticForce();
        }
    }

    void ApplyMagneticForce()
    {
        rb.AddForce(Vector2.up * magneticForce);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
        }
    }

    public void OnPointerDownLeft()
    {
        moveLeft = true;
    }

    public void OnPointerDownRight()
    {
        moveRight = true;
    }

    public void OnPointerUp()
    {
        moveLeft = false;
        moveRight = false;
    }

    public void OnJumpButtonDown()
    {
        if (isGrounded)
        {
            isJumping = true;
        }
    }

    public void OnMagneticClawsButton()
    {
        isUsingMagneticClaws = true; 
    }

    public void OffMagneticClawsButton()
    {
        isUsingMagneticClaws = false;
    }
}
