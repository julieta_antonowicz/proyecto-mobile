using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Proyectil : MonoBehaviour
{
    private Action<Proyectil> DesactivarAccion;
    public int damage = 1; 
    public float lifeTime = 5f; 

    void Start()
    {
        StartCoroutine(DesactivarTiempo());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.GetComponent<CharacterHealth>().TakeDamage(damage);
            DesactivarAccion(this);
        }
    }

    private IEnumerator DesactivarTiempo()
    {
        yield return new WaitForSeconds(lifeTime);
        DesactivarAccion(this);
    }
    public void DesactivarProyectil(Action<Proyectil> desactivarAccionParametro)
    {
        DesactivarAccion = desactivarAccionParametro;

    }
}
