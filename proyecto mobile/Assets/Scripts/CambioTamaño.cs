using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CambioTamaño : MonoBehaviour
{
    public float shrinkScale = 0.5f;
    public float shrinkDuration = 0.5f; // Duración de la animación de reducción
    public Button shrinkButton;

    private Vector3 originalScale;
    private bool isShrunken = false;
    private Coroutine currentCoroutine;

    private void Start()
    {
        originalScale = transform.localScale;
        shrinkButton.onClick.AddListener(ToggleShrink);
    }

    public void ToggleShrink()
    {
        if (currentCoroutine != null)
        {
            StopCoroutine(currentCoroutine);
        }

        if (isShrunken)
        {
            currentCoroutine = StartCoroutine(RestorePlayer());
        }
        else
        {
            currentCoroutine = StartCoroutine(ShrinkPlayer());
        }

        isShrunken = !isShrunken;
    }

    private IEnumerator ShrinkPlayer()
    {
        float timer = 0f;
        Vector3 targetScale = originalScale * shrinkScale;
        while (timer < shrinkDuration)
        {
            timer += Time.deltaTime;
            float t = Mathf.Clamp01(timer / shrinkDuration);
            transform.localScale = Vector3.Lerp(originalScale, targetScale, t);
            yield return null;
        }
        transform.localScale = targetScale;
    }

    private IEnumerator RestorePlayer()
    {
        float timer = 0f;
        Vector3 targetScale = originalScale;
        while (timer < shrinkDuration)
        {
            timer += Time.deltaTime;
            float t = Mathf.Clamp01(timer / shrinkDuration);
            transform.localScale = Vector3.Lerp(transform.localScale, targetScale, t);
            yield return null;
        }
        transform.localScale = targetScale;
    }
}
